package id.web.saungbudaya.sadaya;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by USER on 3/27/2018.
 */

public class signP extends AppCompatActivity {
    TextView Masuk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.login_pengurus);


        Masuk = findViewById(R.id.Masuk1);
        final String[] tempres = {""};
        final RequestQueue MyRequestQueue = Volley.newRequestQueue(this);


        String url = "http://www.blue-banana-bear.xyz/index.php/auth";
        final StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                tempres[0] = response;


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog alertDialog = new AlertDialog.Builder(signP.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Terjadi Kesalahan !");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }) {
            protected Map<String, String> getParams() {
                EditText ntp = (EditText) findViewById(R.id.NIP);
                final String ntps = ntp.getText().toString();
                EditText password = (EditText) findViewById(R.id.password);
                final String passwords = password.getText().toString();
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("username", ntps); //Add the data you'd like to send to the server.
                MyData.put("password", passwords);
                return MyData;
            }
        };










        Masuk.setOnClickListener(new View.OnClickListener() {
            String temp = "";
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                MyRequestQueue.add(MyStringRequest);

                String jabatan = "";
                String login = "false";
                String NTPJ ="";
                String nama =" ";
                try {
                    JSONObject reader = new JSONObject(tempres[0]);
                    login = String.valueOf(reader.getBoolean("login"));
                    jabatan = reader.getString("Jabatan");
                    NTPJ = reader.getString("NTP");
                    nama = reader.getString("Nama");
                } catch (JSONException e) {
                    e.printStackTrace();
                }







                if(login=="true"){
                    Bundle b = new Bundle();
                    b.putString("nama_session", nama); //Your id
                    b.putString("NTP_session",NTPJ);
                    b.putString("jabatan_session",jabatan);

                    Intent it = new Intent(signP.this,MainActivity.class);
                    it.putExtras(b);//Put your id to your next Intent
                    startActivity(it);
                }else{
                    AlertDialog alertDialog = new AlertDialog.Builder(signP.this).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Username/Password Salah!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                }





            }
        });
    }


    }



