package id.web.saungbudaya.sadaya;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by USER on 4/18/2018.
 */

public class absensi extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_absen);
        getSupportActionBar().setTitle("Absensi Divisi Angklung");
        TextView absen = findViewById(R.id.absenAngklung);
        final String[] tempres = {""};
        final RequestQueue MyRequestQueue = Volley.newRequestQueue(this);


        String url = "http://www.blue-banana-bear.xyz/index.php/divisi/presensi";
        final StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                tempres[0] = response;


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog alertDialog = new AlertDialog.Builder(absensi.this).create();
                alertDialog.setTitle("Absensi");
                alertDialog.setMessage("Absensi Berhasil !");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }) {
            protected Map<String, String> getParams() {
                EditText nta = (EditText) findViewById(R.id.NTA_Angklung);
                final String ntas = nta.getText().toString();
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("NTA", ntas); //Add the data you'd like to send to the server
                MyData.put("DAID","171801");

                return MyData;
            }
        };


        absen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyRequestQueue.add(MyStringRequest);
                AlertDialog alertDialog = new AlertDialog.Builder(absensi.this).create();
                alertDialog.setTitle("Absensi Berhasil!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();




            }
        });



    }
}
